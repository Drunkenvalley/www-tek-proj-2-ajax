-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 02, 2014 at 09:42 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `www-tech`
--
CREATE DATABASE IF NOT EXISTS `www-tech` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `www-tech`;

-- --------------------------------------------------------

--
-- Table structure for table `dgst_pages`
--

CREATE TABLE IF NOT EXISTS `dgst_pages` (
  `pageID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `pageName` varchar(128) NOT NULL COMMENT 'Title of page',
  `content` text NOT NULL,
  `ownerID` int(11) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastUpdatedBy` int(11) DEFAULT NULL,
  `lastUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pageID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `dgst_pages`
--

INSERT INTO `dgst_pages` (`pageID`, `pageName`, `content`, `ownerID`, `createdOn`, `lastUpdatedBy`, `lastUpdated`) VALUES
(1, 'Home', '<h1>This is a triumph.</h1>\r\n<h2>Subtext goes here.</h2>\r\n<p>And here we go, this is text. Who''d have thought.</p>\r\n<p>And here we go, this is text. Who''d have thought.</p>\r\n<p>And here we go, this is text. Who''d have thought.</p>', 1, '2014-04-26 17:38:27', NULL, '2014-04-26 17:38:27');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
