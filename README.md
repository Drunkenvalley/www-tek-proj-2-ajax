# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is a CMS made by Daniel Granerud and Sveinung Tørresdal as a comprehensive project for a subject, 'WWW-tek', which explores the use and manipulation of websites to create end-user experiences.
* Early alpha.
* [Live version](http://drunkenvalley.com/dgst)

### Who do I talk to? ###

* Sveinung Tørresdal, also known as Drunkenvalley, maintains this repository. You can reach him at drunkenvalley(at)gmail.com.