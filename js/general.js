/*
SECTION: document.ready
What to do when the page has finished loading.
*/
{
$(document).ready(function() {
    listeners();
    link_response();
	user_load();
    get_menu();
});
}

/*
SECTION: EVENT LISTENERS
This section is entirely dedicated to detecting actions done to the page, and the code they result in.
*/
{
function listeners() {
    //This function just functions as a 'hub' for the rest.
    
    //User info options
    listener_edit_profile();
    listener_logout();
    listener_file_browser();
    listener_file_dialog();
    listener_reset_password();
    
    //Toolbox listeners
    listener_add_page();
    listener_save_page();
    listener_delete_page();
    listener_public_page_toggle();
    listener_comments_page_toggle();
    listener_edit_page_toggle();
    
    //Comments
    listener_comment_submit();
    listener_comment_sort();
    //listener_comment_report();
    
    //Misc
    listener_links();
}

/*
--SUBSECTION: USERFIELD BUTTONS
Buttons that are next to the user's avatar. Edit, logout, upload, etc.
*/
{
function listener_edit_profile() {
    $("body").on("click","#edit_profile", function() {
        var request = $.ajax({
            url: "php/dgst_edit_user.php"

        });
        request.done(function(data) {
            $("#backdrop").children().remove();
            $("#backdrop").append(data);
        });
    });
}

function listener_file_browser() {
    $("body").on("click","#file_browser", function() {
        var request = $.ajax({
            url: "php/dgst_file_browser_dialog.php"

        });
        request.done(function(data) {
            $("#backdrop").children().remove();
            $("#backdrop").append(data);
        });
    });
}

function listener_file_dialog() {
    $("body").on("click","#upload_file", function() {
        var request = $.ajax({
            url: "php/dgst_upload_dialoge.php"

        });
        request.done(function(data) {
            $("#backdrop").children().remove();
            $("#backdrop").append(data);
        });
    });
}

function listener_reset_password() {
    $("body").on("click","#reset_password",function() {
        var request = $.ajax({
            url:"php/dgst_reset_password.php"
        });
        request.done(function(data) {
            $("#backdrop").children().remove();
            $("#backdrop").append(data);
        });
    });
}

function listener_logout() {
    $("body").on("click","#logout", function() {
        var request = $.ajax({
                url:"php/dgst_user_functions.php",
                type: "get",
                data: "page=logout"
        })
        request.done(function(response) {
            user_load();
            get_menu();
            link_response();
        });
    });
}
}

/*
--SUBSECTION: TOOLBOX
Buttons that are loaded for a given page. Create, save, toggle public, etc.
*/
{
function listener_add_page() {
    $("body").on("click","#add_page",function() {
        var request = $.ajax({
            url: "php/dgst_page.php",
            type: "get",
            data: "functionName=create_page"
        });
        request.done(function(data) {
            history.pushState(null,null,"./?pageID="+data);
            get_menu();
            get_page(data);
            get_comments();
        });
    });
}

function listener_save_page() {
    $("body").on("click","#save_page",function() {
        var pageID = document.URL.substring(document.URL.search("pageID=")+7).split("&")[0];
        var title = tinyMCE.get("current_page").getContent();
        var content = tinyMCE.get("page").getContent();

        var request = $.ajax({
            url:"php/dgst_page.php",
            type: "post",
            data: {functionName: "save_page", save_id: pageID, save_title: title, save_content: content}
        });
        
        request.done(function(data) {
            get_menu();
            get_page();
        });
    });
}

function listener_delete_page() {
    $("body").on("click","#delete_page",function() {
        var pageID = document.URL.substring(document.URL.search("pageID=")+7).split("&")[0];
        
        var request = $.ajax({
            url: "php/dgst_page.php",
            type: "get",
            data: {functionName: "delete_page", delete_page: pageID}
        });
        request.done(function(data) {
            get_menu();
            get_page();
        });
    });
}

function listener_public_page_toggle() {
    $("body").on("click","#publish_page",function(event) {
        var pageID = document.URL.substring(document.URL.search("pageID=")+7).split("&")[0];
        var set_public = 1;
        if($("#publish_page").parent().hasClass("inactive")) {
            set_public = 0;
        }
        
        var request = $.ajax({
            url:"php/dgst_page.php",
            type: "get",
            data: {functionName: "toggle_public", togglePage: pageID, togglePublic: set_public}
        });
        
        request.done(function(data) {
            get_page();
        });
    });
}

function listener_comments_page_toggle() {
    $("body").on("click","#has_comments",function(event) {
        var pageID = document.URL.substring(document.URL.search("pageID=")+7).split("&")[0];
        var set_comments = 0;
        if($("#has_comments").parent().hasClass("inactive")) {
            set_comments = 1;
        }
        
        var request = $.ajax({
            url:"php/dgst_page.php",
            type: "get",
            data: {functionName: "toggle_comments", togglePage: pageID, toggleComments: set_comments}
        });
        
        request.done(function(data) {
            get_page();
        });
    });
}

function listener_edit_page_toggle() {
    $("body").on("click","#edit_page",function(event) {
        var request = $.ajax({
            url:"php/dgst_page.php",
            type: "get",
            data: {functionName: "toggle_edit"}
        });
        
        request.done(function(data) {
            get_page();
        });
    });
}
}

/*
--SUBSECTION: COMMENTS
Any events in the comment section.
*/
{
function listener_comment_submit() {
    $("body").on("submit","#submitComment",function(event) {
        event.preventDefault();
        var page_id = document.URL.substring(document.URL.search("pageID=")+7).split("&")[0];
        var content = tinyMCE.get("commentBody").getContent();
        //console.log(content);
        var request = $.ajax({
            url: "php/dgst_page.php",
            type: "post",
            data: {functionName: "post_comment", commentParent: page_id, content: content}
        });
        request.done(function(data) {
            console.log(data);
            get_page(page_id);
        });
    });
}

function listener_comment_sort() {
    $("body").on("change","#commentSorting",function() {
        get_comments();
    });
}

function listener_comment_report() {
    $("body").on("click",".report_comment",function() {
        event.preventDefault();
        var commentID = $(this).attr("name");
        console.log("response: "+commentID);
        var request = $.ajax({
            url: "php/dgst_page.php",
            data: {functionName: "report_comment", reported_comment: commentID}
        });
        request.done(function(data) {
            console.log("response: "+data);
        });
    });
}
}

/*
--SUBSECTION: MISC
Any lone wolves.
*/
{

function listener_links() {
    $("body").on("click","a",function(event){
        var href = $(this).attr("href");
        if(href == ".") {
            event.preventDefault();
        }
        if(href.search("./") == 0) {
            event.preventDefault();
            history.pushState(null, null, href);
            link_response();    
        }
    });
}
}
}

/*
SECTION: BITS OF PAGE LOADING
These functions load content of the page.
*/
{
function link_response() {
    var php_request = document.URL.split("/?")[1];
    if(php_request) {
        if(php_request.search("pageID=") != -1) {
            var href = php_request.substring(php_request.search("pageID=")+7).split("&")[0];
                //This will take the PHP requests (ex: ?page=bla&pageID=2&logout),
                //strip away content preceeding the pageID=, then stripping away
                //anything after the numeric ID we want, by splitting the string 
                //wherever '&' occurs and ONLY taking the very first piece.
                //TL;DR: ?page=bla&pageID=2&logout will be reduced to just 2.
                        
            get_page(href);
        }
    }
    else {
        history.pushState(null,null,"./?pageID=1");
        get_page();
    }
}

function get_menu() {
    var page_id;
    if(document.URL.search("pageID") != -1) {
        page_id = document.URL.substring(document.URL.search("pageID=")+7).split("&")[0];
    }

    var request = $.ajax({
        url:"php/dgst_page.php",
        type: "get",
        dataType: "json",
        data: {functionName: "get_menu"}
    });
    
    request.done( function(data) {
        tinymce.remove(".edit_title");
        $("#userpages").children().remove();
        for(var i=0; i < data.length; i++) {
            if(data[i]["pageID"] == page_id) {
                $("#userpages").append("<li><span id='current_page'>"+data[i]["pageName"]+"</span></li>");
                if($("#page").hasClass("edit_content")) {
                    $("#current_page").addClass("edit_title");
                    tiny_init("title");
                }
            }
            else {
                $("#userpages").append("<li><a href='./?pageID="+data[i]["pageID"]+"'>"+data[i]["pageName"]+"</a></li>");
            }
        }    
        $("#userpages")
    });
}

function get_page(href) {
    if(!href) {
        var href = document.URL.substring(document.URL.search("pageID=")+7).split("&")[0];
    }
    
    var request = $.ajax({
        url:"php/dgst_page.php",
        type: "get",
        dataType: "json",
        data: {functionName: "get_page",pageID:href}
    });
    
    request.done( function(data) {
        tinymce.remove(".edit_content");
        $("#content").children().remove();
        
        if(data.canEdit == 1) {
            $("#content").append("<div class='edit_content' id='page'>"+data.content+"</div>");
            tiny_init("content");
        }
        else {
            $("#content").append("<div id="+data.pageID+">"+data.content+"</div>");
        }
        
        get_menu();
        get_tools();
        
        
        get_comments();
        
    });
    
    request.fail(function() {
        $("#toolbox").children().remove();
        $("#toolbox").append("<li class='special'><a href='.' id='add_page'><img src='content/add_page.png' title='Add new page'  /></a></li>");
        error_404();
    });
}

function get_tools() {
    var page_id = document.URL.substring(document.URL.search("pageID=")+7).split("&")[0];
    $("#toolbox").children().remove();
    
    var request = $.ajax({
        url: "php/dgst_page.php",
        dataType: "json",
        type: "get",
        data: {functionName: "get_tools", currentPage: page_id}
    });
    request.done(function(data) {
        $.each(data,function(i,item) {
            $("#toolbox").append("<li class='special'><a href='.' id="+item.id+"><img src="+item.img+" title="+item.title+" />");
            if(item.state == 'inactive') {
                $("#"+item.id).parent().addClass('inactive');
            }
        });
    });
}

function get_comments() {
    var href = document.URL.substring(document.URL.search("pageID=")+7).split("&")[0];;
    var sorting = $("#commentSorting").val();
    if(!sorting) {sorting = "desc";}
    tinymce.remove("#commentBody");
    $("#comments").children().remove();
    
    var request = $.ajax({
        url: "php/dgst_page.php",
        dataType: "json",
        type: "get",
        data: {functionName: "get_comments", commentParent: href}
    });
    request.done(function(data){
        
        
        
        if(data.length == 0) {
            
        }
        else {
            $("#comments").append("<h1>Comments:</h1>");
            if(data.no_comments) {
                $("#comments").append("<p>No comments exist.");
            }
            else {
                $("#comments").append("<span>Sort comments by: <select id='commentSorting'>");
                if(sorting == "asc") {
                    $("#commentSorting").append("<option value='desc'>Descending");
                    $("#commentSorting").append("<option value='asc' selected>Ascending");
                }
                else {
                    $("#commentSorting").append("<option value='desc' selected>Descending");
                    $("#commentSorting").append("<option value='asc'>Ascending");
                }
            
                $("#comments").append("<table id='pageComments' sortable>");
            }
            
            if(sorting == "desc") {
                for (var current = data.length-1; current > (-1); current--) {
                    $("#pageComments").append("<tr><td class='profile_picture'><img class='user' src="+data[current].userImage+" /></td><td class='comment_content'><h1>"+data[current].userName+"</h1><content>"+data[current].content+"</content><span class='time'>"+data[current].time);
                };
            }
            else {
                for (var current = 0; current < data.length; current++) {
                    $("#pageComments").append("<tr><td class='profile_picture'><img class='user' src="+data[current].userImage+" /></td><td class='comment_content'><h1>"+data[current].userName+"</h1><content>"+data[current].content+"</content><span class='time'>"+data[current].time);
                };
            }
            
            //This might be cheeky, but I wanted to keep it user-side for this if possible.
            //Therefore we test to see if an element that only occurs when logged in exists.
            if($("#user").find("#logged_in").length) {
                $("#comments").append("<form id='submitComment' method='post' action='php/dgst_page.php'>");
                    $("#submitComment").append("<input type='hidden' name='functionName' value='post_comment' />");
                    $("#submitComment").append("<textarea id='commentBody'></textarea>");
                    $("#submitComment").append("<input type='submit' value='Submit comment' class='button right' />");
                    $("#submitComment").append("<span class='bfc'>&nbsp;</span>")
                    
                tiny_init("textarea");
            }
        }
    });
}

function user_load() {
    var request = $.ajax({
        url:"php/dgst_user_login.php",
    })
    request.done(function(data) {
        $("#user").children().remove();
        $("#user").append(data);
        var msg = user_login();	//Because the user_login fell out of scope of document.ready
                                //we have to run this function to get the callback to find #loginForm
    });
}

}

/*
SECTION: MISC
For functions that serve other purposes. These functions don't really tie into a group, so they're here instead.
*/
{
function user_login() {
    $("#loginForm").submit(function(event) {
        event.preventDefault();
        var request = $.ajax({
            url: "php/dgst_user_functions.php",
            type: "post",
            data: $("#loginForm").serialize()
        });
        
        request.done(function(data) {
            if(data == "true") {
                user_load();	//Reloads the content, returning user-info if logged in
                get_menu();
                link_response();
            }
            else {
                $("#error").remove();
                $("#user").append("<div id='error'>Wrong combination.<br/><a href='.' id='reset_password'>Reset password?</a></div>");
            }
        });
        
    });
}
    
function error_404() {
    $("#content").children().remove();
    $("#content").append("<div>This page does not exist, or has been deleted.</div>");
}

function tiny_init(string) {
    if(string == "title") {
        tinymce.init({
            selector: ".edit_title",
            inline: true,
            toolbar: "undo redo",
            menubar: false
        });
    }

    if(string == "content") {
        tinymce.init({
            selector: ".edit_content",
            inline: true,
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        });
    }
    
    if(string == "textarea") {
        tinymce.init({
            selector: "#commentBody",
            inline: false,
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        });
    }
}

}