-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 14, 2014 at 09:24 PM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `www-tech`
--

-- --------------------------------------------------------

--
-- Table structure for table `dgst_comments`
--

CREATE TABLE IF NOT EXISTS `dgst_comments` (
  `commentID` int(11) NOT NULL AUTO_INCREMENT,
  `postID` int(11) NOT NULL,
  `userID` bigint(20) DEFAULT NULL,
  `content` text COLLATE utf8_bin NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reported` tinyint(4) NOT NULL DEFAULT '0',
  `reported_by` int(11) DEFAULT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`commentID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `dgst_files`
--

CREATE TABLE IF NOT EXISTS `dgst_files` (
  `fileID` int(11) NOT NULL AUTO_INCREMENT,
  `fileName` varchar(2048) NOT NULL,
  `originalName` varchar(1024) NOT NULL,
  `userID` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`fileID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dgst_pages`
--

CREATE TABLE IF NOT EXISTS `dgst_pages` (
  `pageID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `pageName` varchar(128) NOT NULL COMMENT 'Title of page',
  `content` text NOT NULL,
  `ownerID` int(11) NOT NULL,
  `lastUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lastUpdatedBy` int(11) DEFAULT NULL,
  `isPublic` tinyint(1) NOT NULL DEFAULT '0',
  `hasComments` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pageID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dgst_users`
--

CREATE TABLE IF NOT EXISTS `dgst_users` (
  `userID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `userName` varchar(64) NOT NULL,
  `password` varchar(256) NOT NULL,
  `temp_password` tinytext COMMENT 'In case of password being reset, but sending email fails.',
  `email` varchar(128) NOT NULL,
  `privilege` tinyint(4) NOT NULL DEFAULT '2',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `image` varchar(4096) NOT NULL DEFAULT 'thumbs/no_avatar.png',
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
