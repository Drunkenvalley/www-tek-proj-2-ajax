<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        
        <link rel="stylesheet" type="text/css" href="content/dgstcms.css">
        <link rel="icon" type="image/png" href="content/favicon.png" />
        <title>dgst|CMS</title>
        
        <script src="js/jquery-1.11.0.min.js"></script>
        <script src="js/general.js"></script>
        <script type="text/javascript" src="js/tinymce/tinymce.min.js"></script>
	</head>
    <body>
        <div id="site">
            <header id="header">
                <img src="content/logo.png" />
                
                <aside id="user">
					<!-- User's info -->
                </aside>
            </header>
            
            <section id="menu">
                <ul>
                    <section id="userpages">
                    
                    <!-- Pages a user has made are loaded here. -->
                    
                    </section>
                    <section id="toolbox">
                    
                    <!-- Buttons relevant to the page we are on. Ie public, groups, etc. -->
                        
                    </section>
                    <li class="bfc"></li>
                </ul>
            </section>
            <section id="taskbar"></section>
			<content id="content">
            
            <!--Content goes here-->
            
            </content>
            <section id="comments">
                
            <!-- Comments go here. -->

            </section>
            <content id="backdrop">
                
            </content>
        </div>
    </body>
</html>