<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>jQuery UI Dialog - Modal form</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
  <script src="js/jquery-1.11.0.min.js"></script>
  <script src="js/jquery-ui-1.10.4.min.js"></script>
  <style>
    form {font-size: 62.5%; }
    form label, input { display:block; }
    form input.text { margin-bottom:12px; width:95%; padding: .4em; }
    form fieldset { padding:0; border:0; margin-top:25px; }
    form h1 { font-size: 1.2em; margin: .6em 0; }
    form .ui-dialog .ui-state-error { padding: .3em; }
    form .validateTips { border: 1px solid transparent; padding: 0.3em; }
  </style>
  <script>
  $(function() {
    var imageFile = $( "#imageFile" ),
      allFields = $( [] ).add( imageFile ),
      tips = $( ".validateTips" );
 
    function updateTips( t ) {
      tips.text( t ).addClass( "ui-state-highlight" );
      setTimeout(function() {
        tips.removeClass( "ui-state-highlight", 1500 );
      }, 500 );
    }

    $("body").on("change","#upload",function() {
        file = this.files[0];
        name = file.name;
        size = file.size;
        type = file.type;        
        console.log(file);
    });
    
    $( "#upload_file_dialoge" ).dialog({
      autoOpen: true,
      modal: true,
      buttons: {
        "Upload": function() {
            var my_dialog = $( this );
            
            if(name.length < 1) {
                updateTips("File invalid.")
            }
            else if(size > 4194304) {
                updateTips("File too large.")
            }
            else if(type != "image/png" && type != "image/gif" && type != "image/jpeg") {
                updateTips("File not right format.")
            }
            else {
                var formData = new FormData($("#uploadForm")[0]);
                $.ajax({
                    url: "php/dgst_file_upload.php",
                    type: "post",
                    success: completeHandler = function(data) {
                        if(data == "success") {
                            my_dialog.dialog("close");
                        }
                    },
                    error: function() {
                        updateTips("Failed to reach script.");
                    },
                    // Form data
                    data: formData,
                    //Options to tell JQuery not to process data or worry about content-type
                    cache: false,
                    contentType: false,
                    processData: false
                }, 'json');
                
                
            }
        },
        Cancel: function() {
          $( this ).dialog( "close" );
        }
      },
      close: function() {
        allFields.val( "" ).removeClass( "ui-state-error" );
      }
    });
  });
  </script>
</head>
<body>
 
<div id="upload_file_dialoge" title="Upload user image">
  <p class="validateTips">All form fields are required.</p>
 
    <form enctype="multipart/form-data" method="post" id="uploadForm" name="uploadForm">
        Upload a picture.<br/>
        <input type="file" id="upload" name="upload" required />
    </form>
</div>
</body>
</html>