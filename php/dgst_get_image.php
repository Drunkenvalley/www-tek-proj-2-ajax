<?php
require_once("dgst_pdo.php");

$id = $_GET['id'];

$db = openDB();
$sql = "SELECT image FROM dgst_users WHERE userID=:id";
$statement = $db->prepare( $sql);
$statement->bindParam(':id', $id);
$statement->execute();

$row = $statement->fetch();

if(!empty($row['image'])) {
	echo $row['image'];
}
else{
	$im = imagecreatefrompng('content/no_avatar.png');
	echo $im;
}
?>