<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>jQuery UI Dialog - Modal form</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
  <script src="js/jquery-1.11.0.min.js"></script>
  <script src="js/jquery-ui-1.10.4.min.js"></script>
  <style>
    form {font-size: 62.5%; }
    form label, input { display:block; }
    form input.text { margin-bottom:12px; width:95%; padding: .4em; }
    form fieldset { padding:0; border:0; margin-top:25px; }
    form h1 { font-size: 1.2em; margin: .6em 0; }
    form .ui-dialog .ui-state-error { padding: .3em; }
    form .validateTips { border: 1px solid transparent; padding: 0.3em; }
  </style>
  <script>
  $(function() {
    $( "#reset_pw_dialog" ).dialog({
        autoOpen: true,
        modal: true,
        buttons: {
            "Reset password": function() {
                var my_dialog = $( this );
                var userName = $("#resetName").val();
                $.ajax({
                    url: "php/dgst_user_functions.php",
                    type: "get",
                    data: {functionName: "reset_PW", userReset: userName},
                    success: function(data) {
                        if(data == "success") {
                            my_dialog.dialog("close");
                        }
                    },
                    error: function() {
                        console.log("Failed to reach script.");
                    },
                });
            },
            Cancel: function() {
              $( this ).dialog( "close" );
            }
        },
        close: function() {
            $("#resetName").val( "" ).removeClass( "ui-state-error" );
        }
    });
  });
  </script>
</head>
<body>
<div id="reset_pw_dialog" title="Reset password">
    Username:<br/>
    <input type="text" name="resetName" id="resetName" />
</div>
</body>
</html>