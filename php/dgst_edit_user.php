<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>jQuery UI Dialog - Modal form</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
  <script src="js/jquery-1.11.0.min.js"></script>
  <script src="js/jquery-ui-1.10.4.min.js"></script>
  <style>
    #register_dialog_form {font-size: 62.5%; }
    #register_dialog_form label, input { display:block; }
    #register_dialog_form input.text { margin-bottom:12px; width:95%; padding: .4em; }
    #register_dialog_form fieldset { padding:0; border:0; margin-top:25px; }
    #register_dialog_form h1 { font-size: 1.2em; margin: .6em 0; }
     #register_dialog_form  .ui-dialog .ui-state-error { padding: .3em; }
     #register_dialog_form  .validateTips { border: 1px solid transparent; padding: 0.3em; }
  </style>
  <script>
    $(function() {
        var email = $( "#email" ),
            password = $( "#password" ),
            password2 = $( "#password2" ),
            img = $("#user_image"),
            allFields = $( [] ).add( name ).add( email ).add( password ),
            tips = $( ".validateTips" );

        function updateTips( t ) {
            tips.text( t ).addClass( "ui-state-highlight" );
            setTimeout(function() {
                tips.removeClass( "ui-state-highlight", 1500 );
            }, 500 );
        }

        $( "#update_dialog_form" ).dialog({
            autoOpen: true,
            modal: true,
            buttons: {
                "Update account": function() {
                    var page_id = document.URL.substring(document.URL.search("pageID=")+7).split("&")[0];

                    var my_dialog = $( this );
                    var ajaxPassword = password.val();
                    var ajaxMail = email.val();
                    var ajaxImg = img.val();
                    $.ajax({
                        type:"POST",
                        url: 'php/dgst_user_functions.php',
                        data:{
                            functionName: 'update_user', 
                            password:ajaxPassword, 
                            eMail:ajaxMail,
                            img: ajaxImg
                        }
                    }).error(function(){
                        
                      updateTips("An error occured")
                    }).success(function(responseData, textStatus, jqXHR) {
                        if(responseData == "true"){
                            updateTips("Success!");
                            user_load();
                            get_comments(page_id);
                            my_dialog.dialog("close");
                        }else if(responseData == "too_large"){
                            updateTips("Image was too large.")
                        }else {
                            updateTips(responseData);
                        }
                    });
                }
            },
            Cancel: function() {
              $( this ).dialog( "close" );
            },
            close: function() {
                allFields.val( "" ).removeClass( "ui-state-error" );
            }
        });
    });
  </script>
</head>
<body>
 
<div id="update_dialog_form" title="Update user info">
  <p class="validateTips">All form fields are required.</p>
 
  <form>
  <fieldset>
    <label for="email">Email</label>
    <input type="text" name="email" id="email" placeholder="Re-enter email" class="text ui-widget-content ui-corner-all">
    <label for="password">Password</label>
    <input type="password" name="password" id="password" placeholder="Password" class="text ui-widget-content ui-corner-all">
    <label for="password2">Password again</label>
    <input type="password" name="password2" id="password2" placeholder="Re-enter password" class="text ui-widget-content ui-corner-all">
    <label for="user_image">User image</label>
    <input type="text" name="user_image" id="user_image" placeholder="100x100px img" class="text ui-widget-content ui-corner-all">
  </fieldset>
  </form>
</div>
</body>
</html>