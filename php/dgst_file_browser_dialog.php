<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>jQuery UI Dialog - Modal form</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
  <script src="js/jquery-1.11.0.min.js"></script>
  <script src="js/jquery-ui-1.10.4.min.js"></script>
  <style>
    form {font-size: 62.5%; }
    form label, input { display:block; }
    form input.text { margin-bottom:12px; width:95%; padding: .4em; }
    form fieldset { padding:0; border:0; margin-top:25px; }
    form h1 { font-size: 1.2em; margin: .6em 0; }
    form .ui-dialog .ui-state-error { padding: .3em; }
    form .validateTips { border: 1px solid transparent; padding: 0.3em; }
    
    #browser_content {position:relative; overflow:hidden; padding:0px; margin:0px; width:100%;}
    #browser_content li {list-style:none; height:130px; padding:5px;}
    .file {float: left; display:block; font-size:0.6em;}
    .file img {display:block;}
    .file a {display:block; color: #2ea8e5;}
    .bfc {float: none; display:block; overflow:hidden;}
     
  </style>
  <script>
  $(function() {
    var imageFile = $( "#imageFile" ),
      allFields = $( [] ).add( imageFile ),
      tips = $( ".validateTips" );
 
    function updateTips( t ) {
      tips.text( t ).addClass( "ui-state-highlight" );
      setTimeout(function() {
        tips.removeClass( "ui-state-highlight", 1500 );
      }, 500 );
    }
    
    $( "#file_browser_dialog" ).dialog({
      autoOpen: true,
      modal: true,
      buttons: {
        Cancel: function() {
          $( this ).dialog( "close" );
        }
      },
      close: function() {
        allFields.val( "" ).removeClass( "ui-state-error" );
      }
    });
    
  });
  </script>
</head>
<body>
 
<div id="file_browser_dialog" title="Upload user image">
<ul id="browser_content">
<?php
    include_once('dgst_pdo.php');
    session_start();
    $db = openDB();
    
    if(!empty($_SESSION['userID'])) {
        $sql = "
        SELECT *
        FROM dgst_files
        WHERE userID=:userID
        ";
        $request = $db->prepare($sql);
        $request->bindValue(":userID",$_SESSION['userID'],PDO::PARAM_INT);
        $request->execute();
        $result = $request->fetchAll(PDO::FETCH_ASSOC);
        
        foreach($result as $row) {
            $fileName = $row["fileName"];
            $originalName = $row["originalName"];
            
            echo<<<HTML
    <li class="file">
        <img src="./thumbs/$fileName" />
        <a href="./thumbs/$fileName">Thumb link</a>
        <a href="./upload/$fileName">Fullsize link</a>
    </li>
HTML;
        }
    }
    else {
        echo "You are not logged in.";
    }
?>
    
</ul>
</div>
</body>
</html>