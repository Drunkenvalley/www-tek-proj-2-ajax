<?php
include_once("dgst_user_functions.php");

class Page {
	var $db;
	
	function Page() {
		global $db;
		$this->db = $db;
	}
	
	function get_page() {
        if(!empty($_GET['pageID'])) {
            $pageID = $_GET['pageID'];
            $sql = 
                "SELECT *
                FROM dgst_pages
                WHERE pageID = :pageID";
            $request = $this->db->prepare($sql);
            $request->bindValue(":pageID",$pageID,PDO::PARAM_INT);
            $request->execute();
            $result = $request->fetch(PDO::FETCH_ASSOC);
            
            $result['canEdit'] = 0;
            if(!empty($_SESSION['edit_mode'])) {
                $result['canEdit'] = 1;
            }
            
            if($result['isPublic'] == 1) {
                echo json_encode($result);
            }
            elseif($result['isPublic'] == 0 && !empty($_SESSION['userID']) && $_SESSION['userID'] == $result['ownerID']) {
                echo json_encode($result);
            }
            elseif($result['isPublic'] == 0 && !empty($_SESSION['privilege']) && $_SESSION['privilege'] < 1) {
                echo json_encode($result);
            }
        }
	}
	
	function get_pages() {
		if(!empty($_SESSION['userID'])) {
			$sql = 
				"SELECT pageName,content 
				FROM dgst_pages
				WHERE ownerID = :owner";
			$request = $this->db->prepare($sql);
			$request->bindValue(":owner",$page_id,PDO::PARAM_INT);
			$request->execute();
			$result = $request->fetchAll(PDO::FETCH_ASSOC);
			
			foreach($result as $row) {
				echo $row;
			}
		}
		else {
			$sql = 
				"SELECT content
				FROM dgst_pages";
			$request = $this->db->prepare($sql);
			$request->execute();
			$result = $request->fetchAll(PDO::FETCH_ASSOC);
			
			foreach($result as $row) {
				echo $row;
				
			}
		}
        
        //json_encode($result);
	}

    function get_menu() {
        $sql = 
            "SELECT pageID, pageName, ownerID, isPublic
            FROM dgst_pages
            ";
        $request = $this->db->prepare($sql);
        $request->execute();
        $result = $request->fetchAll(PDO::FETCH_ASSOC);
        
        $result_array = array();
       
        foreach($result as $row) {
            if($row['isPublic'] == 1) {
                $result_array[] = $row;
            }
            elseif($row['isPublic'] == 0 && !empty($_SESSION['userID']) && $_SESSION['userID'] == $row['ownerID']) {
                $result_array[] = $row;
            }
            elseif($row['isPublic'] == 0 && !empty($_SESSION['is_admin'])) {
                $result_array[] = $row;
            }
        }
        
        echo json_encode($result_array);
    }
    
    function get_tools() {
        if(!empty($_GET['currentPage'])) {
            $sql = "
            SELECT ownerID, isPublic, hasComments
            FROM dgst_pages
            WHERE pageID=:pageID
            ";
            $request = $this->db->prepare($sql);
            $request->bindValue(":pageID",$_GET['currentPage']);
            $request->execute();
            $result = $request->fetch(PDO::FETCH_ASSOC);
            
            $tools = array();
            if(isset($_SESSION['privilege']) && $_SESSION['privilege'] < 2) {
                $add_page = array(
                        "state"=>"active",
                        "id"=>"add_page",
                        "img"=>"content/add_page.png",
                        "title"=>"Add new page"
                );
                $tools[] = $add_page;
            }
            
            if(isset($_SESSION['privilege']) && $_SESSION['privilege'] < 2 || $result['ownerID'] == $_SESSION['userID']) {                
                if(!empty($_SESSION['edit_mode'])) {
                    $edit_page = array(
                        "state"=>"active",
                        "id"=>"edit_page",
                        "img"=>"content/public_edit.png",
                        "title"=>"Toggle editing"
                    );
                }
                else {
                    $edit_page = array(
                        "state"=>"inactive",
                        "id"=>"edit_page",
                        "img"=>"content/hidden_edit.png",
                        "title"=>"Toggle editing"
                    );
                }
                $tools[] = $edit_page;
                
                if(!empty($_SESSION['edit_mode'])) {
                    if($_SESSION['privilege'] < 1) {
                        $delete_page = array(
                            "state"=>"active",
                            "id"=>"delete_page",
                            "img"=>"content/delete_page.png",
                            "title"=>"Delete page"
                        );
                        $tools[] = $delete_page;
                    }
                    
                    $save_page = array(
                        "state"=>"active",
                        "id"=>"save_page",
                        "img"=>"content/save_page.png",
                        "title"=>"Save page"
                    );
                    $tools[] = $save_page;
                    
                    if(!empty($result['hasComments'])) {
                        $has_comments = array(
                            "state"=>"active",
                            "id"=>"has_comments",
                            "img"=>"content/public_comments.png",
                            "title"=>"Comments enabled"
                        );
                        $tools[] = $has_comments;
                    }
                    else {
                        $has_comments = array(
                            "state"=>"inactive",
                            "id"=>"has_comments",
                            "img"=>"content/hidden_comments.png",
                            "title"=>"Comments disabled"
                        );
                        $tools[] = $has_comments;
                    }
                    if(!empty($result['isPublic'])) {
                        $publish_toggle = array(
                            "state"=>"inactive",
                            "id"=>"publish_page",
                            "img"=>"content/public_page.png",
                            "title"=>"Public page"
                        );
                        $tools[] = $publish_toggle;
                    }
                    else {
                        $publish_toggle = array(
                            "state"=>"active",
                            "id"=>"publish_page",
                            "img"=>"content/hidden_page.png",
                            "title"=>"Hidden page"
                        );
                        $tools[] = $publish_toggle;
                    }
                    
                }
            }
            
            echo json_encode($tools);
        }
    }
    
    function get_comments() {
        if(!empty($_GET['commentParent'])) {
            $test = "SELECT hasComments FROM dgst_pages WHERE pageID=:pageID";
            $test_request = $this->db->prepare($test);
            $test_request->bindValue(":pageID",$_GET['commentParent'],PDO::PARAM_INT);
            $test_request->execute();
            $test_result = $test_request->fetch(PDO::FETCH_ASSOC);
            if($test_result['hasComments'] == 1) {        
                $sql = "
                SELECT 
                dgst_comments.commentID AS commentID,
                dgst_comments.postID AS postID,
                dgst_comments.content AS content,
                dgst_comments.time AS time,
                dgst_comments.deleted AS deleted,
                dgst_users.userID AS userID,
                dgst_users.userName AS userName,
                dgst_users.image AS userImage
                FROM dgst_comments
                JOIN dgst_users
                ON dgst_comments.userID = dgst_users.userID 
                WHERE postID=:postID
                ORDER BY time
                ";
                $request = $this->db->prepare($sql);
                $request->bindValue(":postID",$_GET['commentParent'],PDO::PARAM_INT);
                $request->execute();
                $result = $request->fetchAll(PDO::FETCH_ASSOC);
                
                $outcome = array();
                foreach($result as $row) {
                    if(!empty($_SESSION["userID"]) && $_SESSION['userID'] == $row['userID']) {
                        $row['postedByUser'] = 1;
                    }
                    else {
                        $row['postedByUser'] = 0;
                    }
                    if($row['deleted'] == 0) {
                        $outcome[] = $row;
                    }
                }
                if(count($outcome) == 0) {
                    $outcome['no_comments'] = "true";
                    //This way, we register that comments ARE supposed to exist, without really toying around with our format.
                }
                
                echo json_encode($outcome);
            }
        }
    }
    
    function post_comment() {
        if(!empty($_POST['commentParent']) && !empty($_SESSION['userID']) && !empty($_POST['content'])
        && isset($_SESSION['privilege']) && $_SESSION['privilege'] < 3) {
            
            $sql = "
            INSERT INTO dgst_comments (postID,userID,content)
            VALUES (:postID,:userID,:content)
            ";
            $request = $this->db->prepare($sql);
            $request->bindValue(":postID",$_POST['commentParent'],PDO::PARAM_INT);
            $request->bindValue(":userID",$_SESSION['userID'],PDO::PARAM_INT);
            $request->bindValue(":content",$_POST['content'],PDO::PARAM_INT);
            $request->execute();
            
            echo "success";
        }
    }
    
    function report_comment() {
        if(!empty($_GET['reported_comment']) && !empty($_SESSION['userID'])) {
            $sql = "
            UPDATE dgst_comments
            SET reported=1, reportedBy=:userID
            WHERE commentID=:reportedComment
            ";
            $request = $this->db->prepare($sql);
            $request->bindValue(":userID",$_SESSION['userID'],PDO::PARAM_INT);
            $request->bindValue(":reportedComment",$GET['reported_comment'],PDO::PARAM_INT);
            echo "success";
        }        
    }
    
    function create_page() {
        if(!empty($_SESSION['userID']) 
        && isset($_SESSION['privilege']) && $_SESSION['privilege'] < 2) {
            $title = "New page";
            $text = "This is a new page. You may try to edit by clicking the title or body.";
            $owner = 1;
            $sql = 
                "INSERT INTO dgst_pages (pageName, content, ownerID)
                VALUES (:title, :text,:ownerID)
                ";
            $request = $this->db->prepare($sql);
            $request->bindValue(":title",$title,PDO::PARAM_STR);
            $request->bindValue(":text",$text,PDO::PARAM_STR);
            $request->bindValue(":ownerID",$_SESSION['userID'],PDO::PARAM_INT);
            $request->execute();
            
            $get_id = $this->db->prepare("SELECT LAST_INSERT_ID() FROM dgst_pages");
            $get_id->execute();
            $result = $get_id->fetch(PDO::FETCH_ASSOC);
            if(is_array($result)) {
                foreach($result as $row) {
                    echo $row;
                }
            }
            else {
                echo $result;
            }
            return;
        }
    }
    
    function save_page() {
        if(!empty($_SESSION['userID']) && !empty($_POST['save_id']) && !empty($_POST['save_title']) && !empty($_POST['save_content'])
        && isset($_SESSION['privilege']) && $_SESSION['privilege'] < 2) {
            $sql = 
                "UPDATE dgst_pages
                SET pageName=:pageName, content=:content
                WHERE pageID=:pageID AND ownerID=:ownerID
                ";
            $request = $this->db->prepare($sql);
            $request->bindValue(":pageName",$_POST['save_title'],PDO::PARAM_STR);
            $request->bindValue(":content",$_POST['save_content'],PDO::PARAM_STR);
            $request->bindValue(":pageID",$_POST['save_id'],PDO::PARAM_INT);
            $request->bindValue(":ownerID",$_SESSION['userID'],PDO::PARAM_INT);
            $request->execute();
        }
    }
    
    function delete_page() {
        if(!empty($_SESSION['userID']) && !empty($_GET['delete_page'])
        && isset($_SESSION['privilege']) && $_SESSION['privilege'] < 2) {
            
            if(isset($_SESSION['privilege']) && $_SESSION['privilege'] <= 1) {
                $sql = 
                    "DELETE FROM dgst_pages
                    WHERE pageID=:pageID AND ownerID=:ownerID
                    ";
                $request = $this->db->prepare($sql);
                $request->bindValue(":pageID",$_GET['delete_page'],PDO::PARAM_INT);
                $request->bindValue(":ownerID",$_SESSION['userID'],PDO::PARAM_INT);
                $request->execute();
            }
            elseif(isset($_SESSION['privilege']) && $_SESSION['privilege'] < 1) {
                
                $sql = 
                    "DELETE FROM dgst_pages
                    WHERE pageID=:pageID
                    ";
                $request = $this->db->prepare($sql);
                $request->bindValue(":pageID",$_GET['delete_page'],PDO::PARAM_INT);
                $request->execute();
            }
        }
    }
    
    function toggle_public() {
        if(!empty($_GET['togglePage']) && isset($_GET['togglePublic'])
        && isset($_SESSION['privilege']) && $_SESSION['privilege'] < 2) {
            
            $sql = 
                "UPDATE dgst_pages
                SET isPublic=:isPublic
                WHERE pageID=:pageID
                ";
            $request = $this->db->prepare($sql);
            $request->bindValue(":isPublic",$_GET['togglePublic'],PDO::PARAM_INT);
            $request->bindValue(":pageID",$_GET['togglePage'],PDO::PARAM_INT);
            $request->execute();
        }
    }
    
    function toggle_comments() {
        if(!empty($_GET['togglePage']) && isset($_GET['toggleComments'])
        && isset($_SESSION['privilege']) && $_SESSION['privilege'] < 2) {
            
            $sql = 
                "UPDATE dgst_pages
                SET hasComments=:hasComments
                WHERE pageID=:pageID
                ";
            $request = $this->db->prepare($sql);
            $request->bindValue(":hasComments",$_GET['toggleComments'],PDO::PARAM_INT);
            $request->bindValue(":pageID",$_GET['togglePage'],PDO::PARAM_INT);
            $request->execute();
        }
    }
    
    function toggle_edit() {
        if(isset($_SESSION['privilege']) && $_SESSION['privilege'] < 2) {
            
            if(!empty($_SESSION['edit_mode'])) {
                $_SESSION['edit_mode'] = 0;
                echo "false";
            }
            else {
                $_SESSION['edit_mode'] = 1;
                echo "true";
            }
        }
    }
}
$page = new Page();
if(!empty($_GET['functionName']) || !empty($_POST['functionName'])) {
    if(!empty($_GET['functionName'])) {
        $function_name = $_GET['functionName'];
    }
    else {
        $function_name = $_POST['functionName'];
    }
    
	if(method_exists($page,$function_name)) {
		$page->$function_name();
	}
}
else {
    $page->get_page();
}
?>