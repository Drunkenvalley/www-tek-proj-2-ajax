<?php
require_once('recaptchalib.php');
include_once ('dgst_script_user.php');

class Comment {
    var $db;
    
    function Comment (){
        global $db;
        $this->db = $db;
           //Will add a comment if there is one in Post.
        if( isset($_POST['add_comment'])){
            if(empty($_SESSION['id'])) {
                $privatekey = "6LfbBO8SAAAAAF6f2kOi7ppO4lvmBcM9sGICZFWz";
                $resp = recaptcha_check_answer ($privatekey,
                    $_SERVER["REMOTE_ADDR"],
                    $_POST["recaptcha_challenge_field"],
                    $_POST["recaptcha_response_field"]);
            }

            if(!empty($_SESSION['id']) || empty($_SESSION['id']) && $resp->is_valid) {
                $postID = $_GET['post'];
                if( isset($_SESSION['user']))
                    $userID = $_SESSION['id'];
                else
                    $userID = NULL;
                    
                global $purifier;
                $content = $purifier->purify($_POST['add_comment']);//Maximum security
                
                $sql = 'INSERT INTO dgst_comments (postID, userID, content)
                        VALUES (?,?,?)';
                        
                $statement = $this->db->prepare($sql);
                $statement->execute(array($postID, $userID, $content));
            }
        }
        
        //For reporting comments marked as unproper
        if( isset( $_GET['report']) && $_GET['report'] == "comment" && !empty($_GET['report_id'])){
            $query = 'SELECT postID, userID, reported, deleted from dgst_comments WHERE commentID=:commentID';
            $inquiry = $this->db->prepare($query);
            $inquiry->bindParam(':commentID', $_GET['report_id']);
            $inquiry->execute();
            $test = $inquiry->fetch();
            
            if(!empty($_SESSION['id'])) {
                $sql = 'SELECT user FROM dgst_post WHERE id=:post_id';
                $statement = $this->db->prepare($sql);
                $statement->bindParam(':post_id',$test['postID']);
                $statement->execute();
                $op = $statement->fetch(); //op == original poster, ie who submitted the blog post
            
                if(!empty($test['userID']) && $test['userID'] == $_SESSION['id']
                || !empty($op['user']) && $op['user'] == $_SESSION['id']) {
                    $sql= 'UPDATE dgst_comments SET reported = true, reported_by=:userID, deleted=true WHERE
                    commentID=:commentID';
                    
                    $statement = $this->db->prepare($sql);
                    $statement->bindParam(':userID', $_SESSION['id']);
                    $statement->bindParam(':commentID', $_GET['report_id']);
                    $statement->execute();
                }
                elseif(!empty($_SESSION['admin']) && $test['reported']) {
                    $sql= 'UPDATE dgst_comments SET deleted=true WHERE
                    commentID=:commentID';
                    
                    $statement = $db->prepare($sql);
                    $statement->bindParam(':commentID', $_GET['report_id']);
                    $statement->execute();
                }
                else {
                    $sql= 'UPDATE dgst_comments SET reported = true, reported_by=:userID WHERE
                    commentID=:commentID';
                    
                    $statement = $this->db->prepare($sql);
                    $statement->bindParam(':userID', $_SESSION['id']);
                    $statement->bindParam(':commentID', $_GET['report_id']);
                    $statement->execute();
                }
            }
            
            else {
                $sql= 'UPDATE dgst_comments SET reported = true WHERE
                commentID=:commentID';
                
                $statement = $this->db->prepare($sql);
                $statement->bindParam(':commentID', $_GET['report_id']);
                $statement->execute();
            }
        }
    }
    
    function show_comments ( $postID_call){
        global $url; 
        global $post_user;
        global $post_userID;
        
        //If postID is given by the function call or is set in Post
        if( isset( $postID_call) || isset( $_GET['post'])){
            if(!empty( $postID_call)){
                $postID = $postID_call;
            }else{
                $postID = $_GET['post'];
            }
            
            $sql = 
                "SELECT 
                    dgst_comments.commentID as commentID,
                    dgst_comments.userID AS userID,
                    dgst_users.username AS username,
                    dgst_users.image AS image,
                    dgst_comments.content AS content,
                    dgst_comments.time AS time,
                    dgst_comments.reported AS reported,
                    dgst_comments.reported_by AS reported_by,
                    dgst_comments.deleted AS hidden
                FROM dgst_comments 
                LEFT JOIN dgst_users ON dgst_comments.userID = dgst_users.userID
                WHERE postID=:postID 
                ORDER BY time DESC";
            $statement = $this->db->prepare( $sql);
            $statement->bindValue(':postID',$postID,PDO::PARAM_INT);
            $statement->execute();
            $result = $statement->fetchAll();
            
            //Print out comments
            $noe = 0;
            foreach($result as $comment){
                json_encode($result);
            }
        }
    }
    
    function show_comment ( $commentID ){        
        $sql = 
            "SELECT 
                dgst_comments.commentID as commentID,
                dgst_comments.userID AS userID,
                dgst_users.username AS username,
                dgst_users.image AS image,
                dgst_comments.content AS content,
                dgst_comments.time AS time,
                dgst_comments.reported_by AS reported_by,
                dgst_comments.deleted AS hidden
            FROM dgst_comments 
            LEFT JOIN dgst_users ON dgst_comments.userID = dgst_users.userID
            WHERE commentID=:commentID 
            ORDER BY time DESC";
        $statement = $this->db->prepare( $sql);
        $statement->bindValue(':commentID',$commentID,PDO::PARAM_INT);
        $statement->execute();
        $result = $statement->fetch();
        
        $comment_commentID = $result['commentID'];

        if($result['hidden'] == 0) {
            if(!empty($result['username'])) {
                $comment_username = $result['username'];
            }
            else {
                $comment_username = "Anonymous";
            }
            $comment_content = $result['content'];
            $comment_time = $result['time'];
            global $url;
    
            if(!empty($_SESSION['user']) && $comment_username == $_SESSION['user']) {
                $label = "Delete";
            }

            else {
                $label = "Report";
            }

            json_encode($result);
        }
    }
}
?>