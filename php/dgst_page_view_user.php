<?php

require_once("dgst_comment_class.php");
$comment = new Comment();

if(!empty($_GET['user_id'])) {

    $get_user_posts = "SELECT 
        dgst_post.id AS 'id',
        dgst_post.content AS 'content',
        dgst_post.time AS 'time',
        dgst_post.reported AS 'reported',
        dgst_post.reported_by AS 'reported_by',
        dgst_post.deleted AS 'hidden',
        dgst_users.username AS 'username',
        dgst_users.email AS 'email',
        dgst_users.id AS 'userID'
        FROM dgst_post 
        JOIN dgst_users ON dgst_post.user=dgst_users.id 
        WHERE dgst_post.user=:user
        ORDER BY dgst_post.time DESC"
        ;
    $return_user_posts = $db->prepare( $get_user_posts);
    $return_user_posts->bindParam(':user',$_GET['user_id']);
    $return_user_posts->execute();
    $user_posts = $return_user_posts->fetchAll();
    
    echo<<<HTML
    <header class="full black colorborder box padded room-top room-bottom">User's content</header>    
    <table border="0px">
HTML;

    foreach($user_posts as $post) {
        $post_id = $post['id'];
        $post_content = $post['content'];
        $post_timestamp = $post['time'];
        $post_user = $post['username'];
        $post_email = $post['email'];
        $post_userID = $post['userID'];
        
        $prefix = "?";
        if(!empty($_GET)) {
            $prefix = "&";
        }
        
        $report_link = $url.$prefix."report=post&report_id=".$post_id;
    
        if(!$post['hidden']) {
            if(!empty($_SESSION['user']) && $post_user == $_SESSION['user']) {
                $label = "Delete";
            }
            
            elseif($post['reported'] && !empty($_SESSION['admin']) && $_SESSION['admin']) {
                $label = "Reported!";
            }

            else {
                $label = "Report";
            }

            echo <<<HTML
            <tr>
            <td>
            <aside class="black box author colorborder padded room-bottom room-right">
HTML;
            
            if(!empty($post_userID)) {
                $user_img = "dgst_get_image.php?id=".$post['userID'];
                echo<<<HTML
                    <img src="$user_img" class="box room-bottom" alt="" />
HTML;
            }
            
        echo<<<HTML
            <span class="box"><a href="?page=view_user&user_id=$post_userID">$post_user</a></span>
            </aside>
            </td>

            <td class="full post">
            <content class="full anchor white colorborder box padded">
            $post_content
            </content>
            <footer class="full black colorborder box padded room-bottom">
            <i>Written $post_timestamp</i> 
            //
            <a href="$report_link"><img class="report" src="report.png" alt="" /> $label</a>
            //
            <a href=".?page=view_post&post=$post_id">Comments &raquo;</a></footer>
            </td>
            </tr>
        
HTML;
        }
        
        else {
            $error = "Post #$post_id removed by admin";
            if($post['reported_by'] == $post['userID']) {
                $error = "Post #$post_id removed by user";
            }
            
            echo<<<HTML
                <tr>
                <td><aside class="white colorborder box padded room-bottom deleted">Removed</aside></td>
                
                <td class="full post"><footer class="full white colorborder box padded room-bottom">
                <i>$error</i>
                </footer></td>
                </tr>
HTML;
        }
    }

    //Show comments that are reported here
    $get_user_comments = 'SELECT commentID FROM dgst_comments WHERE userID=:user';
    $return_user_comments = $db->prepare( $get_user_comments);
    $return_user_comments->bindParam(':user',$_GET['user_id']);
    $return_user_comments->execute();
    $user_comments = $return_user_comments->fetchAll();

    foreach($user_comments as $user_comment) {
        $comment->show_comment($user_comment['commentID']);
    }
    
    echo<<<HTML
    </table>
HTML;
}