<?php
include_once('dgst_pdo.php');
session_start();
$user_msg = array();
$db = openDB();

if(!empty($_GET['page']) && $_GET['page'] == 'logout') {
    session_destroy();
    setcookie("user","",1);
    setcookie("password","",1);
} else {
    if(!empty($_POST['login_user']) && !empty($_POST['login_password'])) {
        $user = clean_input($_POST['login_user']);
        $password = crypt(clean_input($_POST['login_password']),CRYPT_BLOWFISH);
        user_login($user,$password);
    }

    elseif(!empty($_COOKIE['user']) && !empty($_COOKIE['password'])) {
        $user = $_COOKIE['user'];
        $password = $_COOKIE['password'];
        user_login($user,$password);
    }

}

function user_login($user,$password) {
    global $db;
    global $user_msg;
    $sql = 'SELECT * FROM dgst_users WHERE userName=:user AND password=:password';
    $login = $db->prepare($sql);
    $login->bindParam(':user', $user);
    $password = crypt(clean_input($password),CRYPT_BLOWFISH);
    $login->bindParam(':password', $password);
    $login->execute();
    
    if($result = $login->fetch(PDO::FETCH_ASSOC)) {
        
        setcookie("userName",$user,time()+60*60*24*30,"/","",false,true);
        setcookie("password",$password,time()+60*60*24*30,"/","",false,true);
        $_SESSION['userName'] = $result["userName"];
        $_SESSION['userID'] = $result["userID"];
        $_SESSION['eMail'] = $result["email"];
        $_SESSION['user_img'] = $result["image"];
        $_SESSION['privilege'] = $result['privilege'];
        
		echo "true";
		return true;
    } else {
        echo "false";
		return false;
    }
}

function register_user() {
    if(!empty($_POST['name']) && !empty($_POST['password']) && !empty($_POST['mail'])) {
        //Cleans the input and crypts the password.
        $user = clean_input($_POST['name']);
        $password = crypt(clean_input($_POST['password']), CRYPT_BLOWFISH);
        $mail = clean_input($_POST['mail']);
        global $db;
    
        $sql = 'INSERT INTO dgst_users (userName, password, email)
            VALUES (:user, :password, :email)';
    
        $sth = $db->prepare($sql);
        $sth->bindParam(':user', $user);
        $sth->bindParam(':password', $password);
        $sth->bindParam(':email', $mail);
        $sth->execute();
        return true;
    }
}

function update_user(){
    global $db;
    
    if(!empty($_SESSION['userID'])) {
        $userID = $_SESSION['userID'];
        if(!empty($_POST['password'])) {
            $password = crypt(clean_input($_POST['password']), CRYPT_BLOWFISH);
            $sql = "
            UPDATE dgst_users
            SET password=:password
            WHERE userID=:userID
            ";
            $request = $db->prepare($sql);
            $request->bindValue(":password",$password,PDO::PARAM_STR);
            $request->bindValue(":userID",$userID,PDO::PARAM_INT);
            $request->execute();
        }
        if(!empty($_POST['eMail'])) {
            $email = $_POST['eMail'];
            $sql = "
            UPDATE dgst_users
            SET email=:email
            WHERE userID=:userID
            ";
            $request = $db->prepare($sql);
            $request->bindValue(":email",$email,PDO::PARAM_STR);
            $request->bindValue(":userID",$userID,PDO::PARAM_INT);
            $request->execute();
        }
        if(!empty($_POST['img'])) {
            $img = $_POST['img'];
            
            $max_size = 100;
            
            $width;
            $height;
            list($width, $height) = getimagesize($img);
            
            if($width > $max_size || $height > $max_size) {
            
            }
            else {
                $_SESSION['user_img'] = $img;
                $sql = "
                UPDATE dgst_users
                SET image=:image
                WHERE userID=:userID
                ";
                $request = $db->prepare($sql);
                $request->bindValue(":image",$img,PDO::PARAM_STR);
                $request->bindValue(":userID",$userID,PDO::PARAM_INT);
                $request->execute();
            }
        }
        echo "true";
    }
}

function clean_input(&$input) {
    return htmlspecialchars(stripslashes(trim($input)));
}


function user_logged_in(){ 
    if(isset($_SESSION['userName'])){
        return true;
    } else {
        return false;
    }
}

//Shows the HTML for logging in a user
function show_login() {
    echo<<<HTML
<form id="loginForm" method="post" action="">
<input type="text" name="login_name" placeholder="Name" required autofocus /><br>
<input type="password" name="login_password" placeholder="Password" required /><br>
<input type="submit" value="Log in" />
HTML;
}

function show_user_info(){
    $userID = $_SESSION['userID'];
    $userName = $_SESSION['userName'];
    $userImage = $_SESSION['user_img'];
    
    echo<<<HTML
	<img src="$userImage" alt="Image could not be displayed." />
    <content>
    <span id='logged_in'>Logged in as <a href=".">$userName</a></span>
	<ul>
HTML;
    
    /*
    if(isset($_SESSION['privilege']) && $_SESSION['privilege'] < 1) {
        echo<<<HTML
    <li><a href="./?page=admin">Admin page</a></li>
HTML;
    }
    */

    echo<<<HTML
	<li><a href="." id="edit_profile">Edit profile</a></li>
	<li><a href="." id="file_browser">Filebrowser</a></li>
    <li><a href="." id="upload_file">Upload</a></li>
	<li><a href="." id="logout">Log out</a></li>
    </ul>
    </content>
HTML;
}

function get_user_ID() {
    echo $_SESSION['userID'];
}

function reset_PW() {
    global $db;
    
    if(!empty($_GET['userReset'])) {
        $new_pw = md5(rand());
        $encrypted = crypt($new_pw, CRYPT_BLOWFISH);
        
        $sql = "UPDATE dgst_users
                SET password = :encrypted, temp_password = :new_pw
                WHERE userName = :userName"
        ;
        $sth = $db->prepare($sql);
        $sth->bindParam(':encrypted', $encrypted);
        $sth->bindParam(':new_pw', $new_pw);
        $sth->bindParam(':userName', $_GET['userReset']);
        $sth->execute();
        
        echo "success";
    }
}


if(!empty($_POST['functionName']) || !empty($_GET['functionName'])) {
    if(!empty($_POST['functionName'])) {
        $functionName = $_POST['functionName'];
    }
    else {
        $functionName = $_GET['functionName'];
    }
    
    if(function_exists($functionName)) {
        $functionName();
    }
} 


if(empty($_POST['login_name'])){
}
else{
    user_login($_POST['login_name'], $_POST['login_password']);
}

?>
