<?php
include_once('dgst_user_functions.php');

//Showes the user info if the user is logged inn instead of showing login
if(user_logged_in()){
    show_user_info();
} else {
    show_login();
	include_once('dgst_register_button.php');
}

//Is at the bottum because then we get the register button under the login button.
?>