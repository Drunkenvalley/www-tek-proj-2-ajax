<?php
function openDB(){
    $host = 'localhost';
	$dbname =  'www-tech';
	$username = 'root';
	$password = '';
	
	try {// Attempting to create a PDO object 
		$conn = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	} catch (PDOException $pe) {
		// If connection fails, give error message and exit script
		die("Could not connect to the database $dbname :" . $pe->getMessage());
	}
	// Return PDO object
	return($conn);
}
?>