<?php
    //This file can be called to upload an image to our ../upload/ folder
    //We currently only allow pictures, because anything else is a bit excessive
    //for the time being.

    include_once('dgst_pdo.php');
    session_start();
    $db = openDB();

    foreach($_FILES as $file) {
        //Rather than knowing what the name of the upload input was,
        //foreach will simply pick them with a substitute name ("$file")
        
        $response = array();
        
        //Test that a filename exists
        if(!empty($file["name"])) {
            
            //Not sure what errors this will prompt, but in case there are.
            //If value is 1 it exceeds allowed file-size, is all I know...
            if($file["error"] == 0) {
            
                //Check if user is logged in.
                if(!empty($_SESSION['userName'])) {                
                    //Copy the uploaded file into permanent storage.
                    $filename = $_SESSION['userName']."_".date('Ymdhis')."_".$file["name"];
                    $filepath = "../upload/".$filename;
                    move_uploaded_file($file["tmp_name"],$filepath);
                    
                    $format = strtolower(substr($filename,strlen($filename)-4));
                    
                    $tmp;
                    if($format == ".gif") {
                        $tmp = imagecreatefromgif($filepath);
                    }
                    elseif($format == ".jpg" || $format == "jpeg") {
                        $tmp = imagecreatefromjpeg($filepath);
                    }
                    elseif($format == ".png") {
                        $tmp = imagecreatefrompng($filepath);
                    }

                    //Getting the right scaling
                    $width = imagesx($tmp);
                    $height = imagesy($tmp);
                    
                    $maxWidth = 100;
                    $maxHeight = 100;
                    
                    $temp1 = $height/$maxHeight;
                    $temp2 = $width/$maxWidth;
                    
                    $factor = ( $temp1 > $temp2)? $temp1 : $temp2;
                    
                    $thumbHeight = $height / $factor;
                    $thumbWidth = $width / $factor;

                    $thumb = imagecreatetruecolor($thumbWidth,$thumbHeight);
                    imagealphablending($thumb, false);
                    imagesavealpha($thumb, true); 
                    imagecopyresampled($thumb,$tmp,0,0,0,0,$thumbWidth,$thumbHeight,$width,$height);
                    imagepng($thumb,"../thumbs/".$filename);
                    
                    $sql = "
                        INSERT INTO dgst_files (fileName, originalName, userID)
                        VALUES (:fileName, :originalName, :userID)
                    ";
                    $request = $db->prepare($sql);
                    $request->bindValue(":fileName",$filename,PDO::PARAM_STR);
                    $request->bindValue(":originalName",$file["name"],PDO::PARAM_STR);
                    $request->bindValue(":userID",$_SESSION['userID'],PDO::PARAM_INT);
                    $request->execute();
                    
                    $response["success"] = "success";
                    echo "success";
                    return;
                }
                else {
                    $response["noUser"] = "true";
                    echo "noUser";
                    return;
                }
            
            }
            else {
                $response["error"] = $file["error"];
                echo "error";
                return;
            }
        }
        else {
            $response["isempty"] = "isempty";
            echo "isEmpty";
            return;
        }
    }
?>

